Auteur : David Lebrun
Date remise: 21 novembre 2012
Thème : Nature
Description : Projet CSS en B53
Fichier Readme.txt
===============================


Tout d'abord, j'ai choisi le thèmes de la nature parce que j'aime bien les couleurs qui accompgagne ce 
type de thème et parfait pour le type de projet que nous avions à faire. Évidemment, les couleurs ont 
été basées sur le vert puisque c'est tout simplement la couleur attribué à la nature. De plus, la photo 
me passionne également et c'est pourquoi j'ai pensé qu'il était une bonne idée d'utiliser une de mes photos
comme bannière qui représentait bien le thème du projet. Au départ, je n'avais pas d'idée concrète pour le thème.
J'ai regardé quelque exemples de Zen Garden CSS pour me faire une idée sur ce que j'allais faire. Ainsi, c'est avec 
inspiration que j'ai décidé de le faire sur ce qui englobe la nature(gazon, arbre, bois, terre, etc). 

